(ns pathom-in-a-box.tutorials.weather-api
  "https://pathom3.wsscode.com/docs/tutorial"
  (:require [com.wsscode.pathom3.connect.indexes :as pci]
            [com.wsscode.pathom3.connect.operation :as pco]
            [com.wsscode.pathom3.interface.eql :as p.eql]
            [com.wsscode.pathom3.interface.smart-map :as psm]))

(pco/defresolver ip->lat-long [{:keys [ip]}]
  {::pco/output [:latitude :longitude]}
  {:latitude "159951" :longitude "85134"})

(pco/defresolver latlong->woeid [{:keys [latitude longitude]}]
  {:woeid (rand-int 1000000)})

(pco/defresolver woeid->temperature [{:keys [woeid]}]
  {:temperature (rand-int 40)})

(def temperature-indexes
  (pci/register [ip->lat-long
                 latlong->woeid
                 woeid->temperature]))

(-> (psm/smart-map temperature-indexes {:ip "192.29.213.3"})
    ;:latitude
    :temperature
    )

;;;;

(def temperatures
  {"Recife" 23})

(pco/defresolver temperature-from-city [{:keys [city]}]
  {:temperature (get temperatures city)})

(def indexes
  (pci/register [temperature-from-city]))

(def smart-map (psm/smart-map indexes {:city "Recife"}))
(:city smart-map)
(:temperature smart-map)

(pco/defresolver cold? [{:keys [temperature]}]
  {:cold? (< temperature 20)})

(def city-indexes
  (pci/register [temperature-from-city cold?]))

(p.eql/process
  city-indexes
  {:city "Recife"}
  [:cold?])

(-> (psm/smart-map city-indexes {:city "Recife"})
    ;:temperature
    :cold?
    )

(comment
  "https://syncwith.com/api/metaweather-api#MetaWeater_Alternative_Open_Weather_Map"
  (ip->lat-long {:ip "198.29.213.3"})
  (latlong->woeid {:longitude "-88.0569", :latitude "41.5119"})
  (-> {:ip "192.29.213.3"}
      ip->lat-long
      latlong->woeid
      woeid->temperature)
  )